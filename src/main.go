package main

/*
 * This bot subscribes using eth2 client to new block events.
 * When new block arrives it grabs the graffiti and tries to parse it.
 * If it's a pixel AND it's in the Rocket Pool target zone, broadcast this.
 */

/* TODO
 * - TimeLapse of graffiti beaconchain wall (notice the missing pixels, grr)
 * - aggregate pixel proposals [e.g. last 10 pixels painted]?
 * - track RP-XX vX.Y.Z graffiti and count pixel opportunities wasted
 */

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/namsral/flag"

	// Image handling for rpl logo
	// "github.com/kettek/apng"
	"image"
	"image/color"
	palette "image/color/palette"
	"image/draw"
	"image/gif"
	_ "image/jpeg"
	"image/png"
	"math/rand"

	"github.com/nfnt/resize"

	// set proctitle
	"github.com/erikdubbelboer/gspt"

	"encoding/json"
	"log"
	ohttp "net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	// relative times
	"github.com/mergestat/timediff"

	"github.com/bwmarrin/discordgo"
	"github.com/rs/zerolog"

	// Eth2 stuff
	eth2client "github.com/attestantio/go-eth2-client"
	api "github.com/attestantio/go-eth2-client/api/v1"
	"github.com/attestantio/go-eth2-client/http"
	spec "github.com/attestantio/go-eth2-client/spec"
	_ "github.com/attestantio/go-eth2-client/spec/phase0"

	// ENS lookups
	common "github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	ens "github.com/wealdtech/go-ens/v3"
	big "math/big"
)

type BenVBot struct {
	ds        *discordgo.Session
	channelId string
}

type CompareWall struct {
	// We'll be comparing pixels based on X,Y, so make a (sparse) map for that
	// The stats are for easy lookups and are filled upon load.
	MinX      uint
	MinY      uint
	MaxX      uint
	MaxY      uint
	PixelData map[uint]map[uint]GraffitiWallPixel
}

// Pixels returned by Beaconcha.in
type GraffitiWallPixel struct {
	Color     string `json:"color"`
	Slot      uint
	Validator uint
	X         uint `json:"x"`
	Y         uint `json:"y"`
}

type GraffitiWall struct {
	Status string
	Data   []GraffitiWallPixel
}

type LeaderBoardEntry struct {
	Validator        uint // TODO: ENS lookup, might be involved though
	PixelCount       uint // Total pixels placed
	RPLPixelCount    uint // RPL pixels placed
	BadPixelCount    uint // Mismatched [RPL] pixels
	LastSeen         uint // slot number
	AvgBlockDistance uint // number of slots in between pixels
	// Note the list of slots can be pulled from the GraffitiWallPixels array
	Pixels []*GraffitiWallPixel
}

type LeaderBoard struct {
	// Top pixel placers, ordered by highest pixel count
	Validators      []*LeaderBoardEntry // All validators
	RPLValidators   []*LeaderBoardEntry // Only RPL pixels
	ValidatorLookup map[uint]*LeaderBoardEntry
}

// Name/pubkey lookup
type Validator struct {
	Index      uint64 // 12345
	NodePubkey string // node pubkey, 0x1234
	ENS        string // trink.eth
	NodeName   string // ENS or abbreviated pubkey, e.g. "trink.eth" or "0x1234...abcd"
}

func (v Validator) String() string {
	if len(v.ENS) > 0 {
		return fmt.Sprintf("%d/%s", v.Index, v.ENS)
	}
	return fmt.Sprintf("%d/%s)", v.Index, v.NodePubkey)
}

// Variables used for command line parameters
var (
	Token              string
	BeaconURL          string
	ExecutionURL       string
	CompareURL         string // url to json file/url to compare against
	CompareURLIsURL    bool   // bookkeeping
	IconFilename       string // bot icon for footers
	GuildID            string
	ChannelID          string
	Xoffset            int // how much the comparedata is offset, calculated in updateComparisonData function
	Yoffset            int
	BeaconClient       eth2client.Service
	ExecutionClient    *ethclient.Client
	bb                 *BenVBot
	listenMode         bool          // true: listens (and responds) to messages, false: deaf
	broadcastMode      int           // 0 == none, 1 = Rocket Pool only, 2 = everything
	fullMsgInterval    int64         // amount of second between short and full messages, set to 0 to have full messages all the time
	bgw                *GraffitiWall // last fetched beaconchain wall result
	wallv1             *regexp.Regexp
	wallv2             *regexp.Regexp
	wallImage          *image.RGBA // used for comparing to what we want to have drawn
	fullWallImage      *image.RGBA // from beaconchain, updated when beaconchain wall is fetched
	rplImage           image.Image // scaled Rocket Pool logo image of what we're drawing, should be read-only
	compareWall        *CompareWall
	iconBuffer         bytes.Buffer        // bot footer image
	fullWallPixels     int                 // total pixels in beaconchain wall
	fullWallRPLPixels  int                 // total Rocket Pool pixels in beaconchain wall
	fullWallMisPixels  int                 // total wrong Rocket Pool pixels in beaconchain wall
	fullWallTodoPixels int                 // total Rocket Pool pixels left unpainted (excluding invalid ones)
	pixelsPerDay       int                 // total Rocket Pool pixels painted in the last 24 hours (read: 7200 blocks, since every slot is 12 seconds)
	lastSlot           uint                // updated when new eth blocks arrive
	lastFullMessage    int64               // time.Now().Unix() to keep track of time in between short and long updates, if difference between now and this is > X we do the big one
	leaderBoard        LeaderBoard         // updateStats() keeps this up2date
	validatorCache     map[uint]*Validator // validator index -> validator entry, updated by getValidator

	// Command
	commands = []*discordgo.ApplicationCommand{
		{
			Name:        "rpwall",
			Description: "Display Rocket Pool section of beaconchain Graffitiwall",
		},
		{
			Name:        "rpwall_leaderboard", // // 1-32 character name matching ^[\w-]{1,32}$
			Description: "Displays Graffitiwall leaderboard",
		},
	}
	commandHandlers = map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
		"rpwall":             handleRpWall,
		"rpwall_leaderboard": handleRpLeaderboard,
	}
)

// BeaconChain deposits structures
type BCDeposit struct {
	// There's a lot of stuff in here: "amount","block_number","block_ts","from_address","merkletree_index","publickey","removed","signature","tx_hash","tx_index","tx_input","valid_signature","withdrawal_credentials"
	// We only care about a subselection for now
	Amount                uint64 `json:"amount"`       // 16000000000, 32000000000
	BlockNumber           uint64 `json:"block_number"` // 13937941
	FromAddress           string `json:"from_address"` // node address
	PublicKey             string `json:"publickey"`
	WithdrawalCredentials string `json:"withdrawal_credentials"` // 0x0100...00c0ffee1234
}
type BCDeposits struct {
	Status string      `json:"status"` // OK
	Data   []BCDeposit `json:"data"`   // data array with deposits
}

func getValidator(index uint64) *Validator {
	// Gets validator based on specified index
	DepositsURL := fmt.Sprintf("https://beaconcha.in/api/v1/validator/%d/deposits", index)
	if _, ok := validatorCache[uint(index)]; !ok {
		// See if we can fetch it
		deposits := &BCDeposits{}
		err := getJson(DepositsURL, deposits)
		if err != nil {
			log.Printf("WARNING: Failed to fetch deposits data from beaconcha.in: %s\n", err)
			return nil
		}
		if deposits.Status != "OK" {
			log.Printf("WARNING: Failed to fetch deposits data from beaconcha.in, status not ok: %+v\n", deposits)
			return nil
		}
		log.Printf("Deposits for validator %d fetched: %+v\n", index, deposits)

		// Enter validator info
		if len(deposits.Data) == 0 {
			log.Printf("WARNING: No deposits found for validator %d?! %+v\n", index, deposits)
			return nil
		}
		nodeAddress := deposits.Data[0].FromAddress

		// See if we have ENS
		// This needs the ExecutionClient, so skip if not there.
		reverse := ""
		if ExecutionClient == nil {
			log.Printf("WARNING: No executionclient available -> no ENS lookups!\n")
		} else {
			ea := common.HexToAddress(nodeAddress)
			reverse, err = ens.ReverseResolve(ExecutionClient, ea)
			if err != nil {
				log.Printf("WARNING: Error during ENS lookup for validator %d [%s]? %s\n", index, nodeAddress, err)
			}
			if reverse == "" {
				log.Printf("DEBUG: Validator %d aka node %s has no reverse lookup\n", index, nodeAddress)
			} else {
				fmt.Printf("DEBUG: Validator %d aka node %s has ENS %s\n", index, nodeAddress, reverse)
			}
		}

		// TODO: Determine if this is a RPL node by looking at the transaction, if it was TO 0xDCD51fc5Cd918e0461B9B7Fb75967fDfD10DAe2f it was RPL
		nname := reverse
		if len(nname) == 0 {
			nlen := len(nodeAddress)
			nname = nodeAddress[0:6] + "..." + nodeAddress[nlen-5:nlen-1]
		}
		validator := &Validator{
			uint64(index),
			nodeAddress,
			reverse, // ENS string or empty
			nname,
		}
		validatorCache[uint(index)] = validator
	}
	return validatorCache[uint(index)]
}

func (lbe *LeaderBoardEntry) String() string {
	return fmt.Sprintf("Validator(%d, %d pixels of which %d are in the RPL area, last seen at block %d, avg block distance %d)", lbe.Validator, lbe.PixelCount, lbe.RPLPixelCount, lbe.LastSeen, lbe.AvgBlockDistance)
}

func makeRpWallImage() *bytes.Buffer {
	// Put wall image in a message. Section we care about is where we have the Rocket Pool image basically, so a 50x50 block at a known offset.
	width := 50
	height := 50
	upLeft := image.ZP
	lowRight := image.Point{width, height}

	db := image.Rectangle{upLeft, lowRight}
	dst := image.NewRGBA(db)
	bgcolor := color.RGBA{R: 255, G: 255, B: 255, A: 255}
	offsetPoint := image.Point{Xoffset, Yoffset}
	draw.Draw(dst, db, &image.Uniform{bgcolor}, image.ZP, draw.Src)

	// Draw a greyscale variant of the Rocket Pool logo first.
	bounds := rplImage.Bounds()
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			c := color.GrayModel.Convert(rplImage.At(x, y)).(color.Gray)
			if c.Y < 1 { // skip black pixels
				continue
			}
			brighter := uint8(32)
			if c.Y <= (255 - brighter) {
				c.Y += brighter // make brighter
			}
			if c.Y < 128 {
				continue // skip this dark garbage
			}
			dst.Set(x, y, c)
		}
	}

	draw.Draw(dst, db, fullWallImage, offsetPoint, draw.Over)

	// Now make it a sane size so we can actually see some things
	dst = resize.Resize(1000, 0, dst, resize.NearestNeighbor).(*image.RGBA) // Lanczos3 gives blur, we want pixels!

	var fbuf bytes.Buffer
	png.Encode(&fbuf, dst)
	return &fbuf
}

func handleRpWall(s *discordgo.Session, i *discordgo.InteractionCreate) {
	log.Printf("/rpwall called by %+v || %+v", i.Interaction.Member, i.Interaction.User)

	pngName := "rplwall.png"
	pfpName := "evil.png"
	fbuf := makeRpWallImage()
	f := bytes.NewReader(fbuf.Bytes())
	h := bytes.NewReader(iconBuffer.Bytes())
	embed := &discordgo.MessageEmbed{
		// Timestamp:   time.Now().Format(time.RFC3339), // Discord wants ISO8601; RFC3339 is an extension of ISO8601 and should be completely compatible.
		Title:       "Graffiti Wall Rocket Pool Section",
		Description: fmt.Sprintf("Graffitiwall zoomed at Rocket Pool section with preview drawn in grayscale."),
		Type:        discordgo.EmbedTypeImage,
		Color:       0xE67E22, // Orange
		Image: &discordgo.MessageEmbedImage{
			// URL: "data:image/png;base64," + f64, // doesn't like this :/
			URL:    "attachment://" + pngName, // can not use data:image/png;base64,<base64data>
			Width:  1000,                      // optional
			Height: 1000,                      // optional
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "hacked together by BenV / trink.eth\n",
			IconURL: "attachment://" + pfpName,
		},
	}

	eta := getEtaString()
	msg := fmt.Sprintf("RPL Pixel overview\n\nRPL Pixels ✅: %d\nRPL Pixels ❌: %d\nRPL Pixels TODO: %d, %.2f%% done\nLast 24h: %d px, 🏁 %s\n", fullWallRPLPixels, fullWallMisPixels, fullWallTodoPixels, float64(float64(100.0)-(float64(fullWallTodoPixels)/float64(20.4))), pixelsPerDay, eta)
	irdata := &discordgo.InteractionResponseData{
		Content: msg,
		Embeds: []*discordgo.MessageEmbed{
			embed,
		},
		Files: []*discordgo.File{
			{
				Name:        pngName,
				Reader:      f,
				ContentType: "image/png",
			},
			{
				Name:        pfpName,
				Reader:      h,
				ContentType: "image/png",
			},
		},
	}
	// Make responses ephemeral unless we're in accepted channel
	if ChannelID != i.Interaction.ChannelID {
		log.Printf("Flagging /rpwall response as ephemeral since %s is not our configured channel", i.Interaction.ChannelID)
		irdata.Flags = 1 << 6 // ephemeral messsage
	} else {
		log.Printf("This /rpwall response is permanent since %s is our configured channel", i.Interaction.ChannelID)
	}
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: irdata,
	})
}

func handleRpLeaderboard(s *discordgo.Session, i *discordgo.InteractionCreate) {
	log.Printf("/rpwall_leaderboard called by %+v || %+v", i.Interaction.Member, i.Interaction.User)
	pfpName := "evil.png"
	h := bytes.NewReader(iconBuffer.Bytes())

	top := ""
	for i := 0; i < 10 && leaderBoard.RPLValidators[i] != nil; i++ {
		// Slot [%s](https://beaconcha.in/slot/%s) by [%d](https://beaconcha.in/validator/%d)
		lbe := leaderBoard.RPLValidators[i]
		ago := lastSlot - lbe.LastSeen
		agoTime := timediff.TimeDiff(time.Now().Add(time.Duration(ago*12) * -time.Second))
		top += fmt.Sprintf("**%d**. [%d](https://beaconcha.in/validator/%d) painted %d RPL pixels, last seen [%s](https://beaconcha.in/slot/%d).\n", i+1, lbe.Validator, lbe.Validator, lbe.RPLPixelCount, agoTime, lbe.LastSeen)
		// top += fmt.Sprintf("%d. %d has drawn %d RPL pixels (%d global), last seen %d slots ago.\n", i+1, lbe.Validator, lbe.RPLPixelCount, lbe.PixelCount, lastSlot-lbe.LastSeen)
	}
	log.Printf("Reply lines: \n\n%s\n\n", top)
	log.Printf("Reply length is: %d\n", len(top))

	// Embed descriptions are limited to 4096 characters. There can be up to 25 fields. A field's name is limited to 256 characters and its value to 1024 characters.
	// Apparently discord just ignores it if you send it more, so mind the length :/
	embed := &discordgo.MessageEmbed{
		Title:       "Rocket Pool Graffiti Wall Leaderboard",
		Description: fmt.Sprintf("**Leaderboard of Rocket Pool pixel placers**.\n\n%s\n", top),
		Type:        discordgo.EmbedTypeRich,
		Color:       0xE67E22, // Orange
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "hacked together by BenV / trink.eth\n",
			IconURL: "attachment://" + pfpName,
		},
		/*
			Fields: []*discordgo.MessageEmbedField{
				&discordgo.MessageEmbedField{
					Name:   "Leaderboard Rocket Pool Pixel Placers",
					Value:  top,
					Inline: false,
				},
			},
		*/
	}

	eta := getEtaString()
	msg := fmt.Sprintf("RPL Pixelplacers leaderboard\n\nRPL Pixels ✅: %d\nRPL Pixels ❌: %d\nRPL Pixels TODO: %d, %.2f%% done\nLast 24h: %d px, 🏁 %s\n", fullWallRPLPixels, fullWallMisPixels, fullWallTodoPixels, float64(float64(100.0)-(float64(fullWallTodoPixels)/float64(20.4))), pixelsPerDay, eta)
	irdata := &discordgo.InteractionResponseData{
		Content: msg,
		Embeds: []*discordgo.MessageEmbed{
			embed,
		},
		Files: []*discordgo.File{
			{
				Name:        pfpName,
				Reader:      h,
				ContentType: "image/png",
			},
		},
	}
	// Make responses ephemeral unless we're in accepted channel
	if ChannelID != i.Interaction.ChannelID {
		log.Printf("Flagging /rpwall_leaderboard response as ephemeral since %s is not our configured channel", i.Interaction.ChannelID)
		irdata.Flags = 1 << 6 // ephemeral messsage
	} else {
		log.Printf("This /rpwall_leaderboard response is permanent since %s is our configured channel", i.Interaction.ChannelID)
	}
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: irdata,
	})
	if err != nil {
		log.Printf("Error sending /rpwall_leaderboard response: %s\n", err)
	}
}

func init() {
	gspt.SetProcTitle("BenVBot [init]")
	flag.StringVar(&Token, "token", "", "Bot Token, also through env[TOKEN]")
	flag.StringVar(&BeaconURL, "beacon_url", "http://localhost:5052/", "Beacon API URL, default http://localhost:5052/, also env[BEACON_URL]")
	flag.StringVar(&ExecutionURL, "execution_url", "http://localhost:8545/", "Execution API URL, default http://localhost:8545/, also env[BEACON_URL]. Alternatively pick infura, e.g. https://mainnet.infura.io/v3/mysecret123")
	flag.StringVar(&CompareURL, "compare_url", "graffiti.json", "Graffiti.json file as generated by drawer to compare what we are drawing (for matching only, should be scaled, offsets supplied below)")
	flag.StringVar(&IconFilename, "iconlogo", "benvbot.png", "Icon Logo image that is passed in footers")
	flag.StringVar(&GuildID, "guild", "", "Guild ID. If not passed - bot registers commands globally")
	flag.StringVar(&ChannelID, "channel", "", "Channel ID. If not passed - bot registers commands globally")
	flag.IntVar(&broadcastMode, "broadcast", 1, "0: None, 1: Rocket Pool pixels only, 2: every pixel")
	flag.Int64Var(&fullMsgInterval, "full_message_interval", 5*3600, "Amount of seconds between a full pixel message. Short pixel events will be thrown in the meanwhile. (only for broadcast mode > 0)")
	flag.BoolVar(&listenMode, "listen", false, "True to listen for certain messages, false to only report to slash commands and block events")
	flag.Parse()
	if len(Token) == 0 {
		fmt.Println("Error: no token supplied.")
		return
	}
	// Debug
	// zerolog.SetGlobalLevel(zerolog.TraceLevel)

	// Must compile. at init. Compile. Ugh.
	// Two formats currently accepted, graffitiwall:x:y:#cccccc or gw:xxxyyycccccc - see https://beaconcha.in/graffitiwall
	// Example: graffitiwall:27:938:#DEBFA2
	wallv1 = regexp.MustCompile(`graffitiwall:(?P<x>\d{1,3}):(?P<y>\d{1,3}):#(?P<hexcolor>[a-fA-F0-9]{6})`)
	// Eample: RP-L v1.2.0 (gw:649471ff9c68)
	wallv2 = regexp.MustCompile(`gw:(?P<x>\d{3})(?P<y>\d{3})(?P<hexcolor>[a-fA-F0-9]{6})`)

	rand.Seed(time.Now().UnixNano())
	validatorCache = make(map[uint]*Validator)

	// Load bot iconImage
	iconFile, err := os.Open(IconFilename)
	if err != nil {
		panic(err)
	}
	defer iconFile.Close()
	iconImage, _, err := image.Decode(iconFile)
	png.Encode(&iconBuffer, iconImage)
	if err != nil {
		panic(err)
	}

	// Load comparison data and generate image from it - hardcoded a few things for now taken from graffiti drawer/settings
	if !loadComparisonData() {
		panic("Error loading data from comparison url, we can't work like this!")
	}

	rplImage, err = generateComparisonImage()

	fmt.Printf("Fetched comparison data using %s\n", CompareURL)
	// Dump to console
	imageToConsole(rplImage)
	// spew.Dump(rplImage)
	// Load the image at the proper spot in a clear 1000x1000 canvas
	width := 1000
	height := 1000
	upLeft := image.ZP
	lowRight := image.Point{width, height}
	wallImage = image.NewRGBA(image.Rectangle{upLeft, lowRight})
	bounds := rplImage.Bounds()

	pixels := 0
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			col := rplImage.At(x, y)
			wallImage.Set(Xoffset+x, Yoffset+y, col)
			pixels++
		}
	}

	fmt.Printf("Wrote %d pixels to wallImg for comparison\n", pixels)
	// Encode as PNG.
	f, err := os.Create("rplwallcheck.png")
	if err != nil {
		panic(err)
	}
	if err := png.Encode(f, wallImage); err != nil {
		f.Close()
		panic(err)
	}
	if err := f.Close(); err != nil {
		panic(err)
	}
	lastSlot = 0 // unknown
	pixelsPerDay = 0

	// DEBUG TEST
	fmt.Printf("Pixel match test: %s\n", NewPixel(688, 490, "ffd58f", "", 0)) // (gw:688490ffd58f)
	fmt.Printf("Pixel mismatch test: %s\n", NewPixel(500, 800, "ffd58f", "", 0))
	// fmt.Printf("Validator lookup: %+v\n", getValidator(277159))

	gspt.SetProcTitle("BenVBot [EC init]")
	initEth1()

	gspt.SetProcTitle("BenVBot [CC init]")
	initEth2()

	// DEBUG TEST2
	fmt.Printf("Validator lookup: %s\n", getValidator(331611))

	gspt.SetProcTitle("BenVBot [fetching wall]")
	fetchGraffitiWall()
	gspt.SetProcTitle("BenVBot [main]")
}

func generateTimelapseMessage() *discordgo.MessageSend {
	// TODO: This OOMs the machine right now, so change to creating separate frame images and glue-ing those
	// OR: use different library to generate GIFs.
	if bgw == nil {
		log.Printf("ERROR: No beacon graffiti wall?!")
		return nil
	}
	return nil // OOM issues - revise this to apng or something more memory-efficient please
	upLeft := image.ZP
	maxX := 1000
	maxY := 1000
	lowRight := image.Point{maxX, maxY}
	db := image.Rectangle{upLeft, lowRight}
	dst := image.NewRGBA(db)

	bgcolor := color.RGBA{R: 255, G: 255, B: 255, A: 255}
	draw.Draw(dst, db, &image.Uniform{bgcolor}, image.ZP, draw.Src)

	subimages := []image.Image{
		dst,
	}

	// generate sorted (by slot) array of pixels (by slot)
	pixels := make([]GraffitiWallPixel, 0, len(bgw.Data))
	for _, pixel := range bgw.Data {
		pixels = append(pixels, pixel)
	}
	sort.SliceStable(pixels, func(i, j int) bool {
		return pixels[i].Slot > pixels[j].Slot
	})

	// Draw the slots, first pixel was drawn in slot 6701 afaik
	lastFrame := dst
	log.Printf("Starting timelapse generation - this will take a while....\n")
	for _, pixel := range pixels {
		frame := image.NewRGBA(db)
		draw.Draw(frame, db, lastFrame, db.Min, draw.Over)
		// set pixel
		cr, _ := strconv.ParseUint(pixel.Color[0:2], 16, 0)
		cg, _ := strconv.ParseUint(pixel.Color[2:4], 16, 0)
		cb, _ := strconv.ParseUint(pixel.Color[4:6], 16, 0)
		pcol := color.RGBA{R: uint8(cr), G: uint8(cg), B: uint8(cb), A: 255}
		dst.Set(int(pixel.X), int(pixel.Y), pcol)
		subimages = append(subimages, frame)
		lastFrame = frame
	}
	outGif := &gif.GIF{}
	// quantizer := quantize.MedianCutQuantizer{NumColor: 64}
	// quantizer := quantize.MedianCutQuantizer{}
	// p := make([]color.Color, 0, 256)
	for _, simage := range subimages {
		bounds := simage.Bounds()
		pImg := image.NewPaletted(bounds, palette.Plan9)
		draw.Draw(pImg, bounds, simage, image.ZP, draw.Src)

		// Add new frame to animated GIF
		outGif.Image = append(outGif.Image, pImg)
		outGif.Delay = append(outGif.Delay, 10) // in 100ths of a second, see https://pkg.go.dev/image/gif#GIF
	}

	var gbuf bytes.Buffer // In-memory buffer to store images
	gif.EncodeAll(&gbuf, outGif)

	// write to file
	f, _ := os.Create("timelapse.gif")
	bc, _ := f.Write(gbuf.Bytes())
	fmt.Printf("Wrote timelapse.gif with %d bytes to file\n", bc)
	f.Close()

	//q := bytes.NewReader(gbuf.Bytes())
	//os.WriteFile("timelapse.gif", q, 0644)
	// gbuf.WriteTo(Stdout)

	g := bytes.NewReader(gbuf.Bytes())
	log.Printf("Timelapse generation done, time to try to write to discord.....\n")

	embed := &discordgo.MessageEmbed{
		Description: fmt.Sprintf("Graffiti Wall Timelapse test!"),
		Type:        discordgo.EmbedTypeRich,
	}
	dmsg := &discordgo.MessageSend{
		Content: fmt.Sprintf("Graffiti Wall Timelapse test!"),
		Embed:   embed,
		Files: []*discordgo.File{
			{
				Name:        "timelapse.gif",
				Reader:      g,
				ContentType: "image/gif",
			},
		},
	}
	return dmsg
}

func generateComparisonImage() (image.Image, error) {
	// Based on compareWall we generate an image of it.
	// First figure out minX,minY,maxX,maxY so we can translate to 0,0-(maxX-minX),(maxY-minY)

	if compareWall == nil {
		panic("No comparewall?!") // This should not happen!
	}

	cEmpty := color.RGBA{R: 0, G: 0, B: 0, A: 0}
	upLeft := image.ZP
	maxX := compareWall.MaxX + 1 // max coordinates of rectangle are excluding
	maxY := compareWall.MaxY + 1
	lowRight := image.Point{int(maxX - compareWall.MinX), int(maxY - compareWall.MinY)}
	db := image.Rectangle{upLeft, lowRight}
	dst := image.NewRGBA(db)

	// Iterate over pixeldata, note this is offset!
	for x := compareWall.MinX; x <= compareWall.MaxX; x++ {
		xrow := compareWall.PixelData[x]
		for y := compareWall.MinY; y <= compareWall.MaxY; y++ {

			if xrow == nil || xrow[y].Color == "" {
				dst.Set(int(x-compareWall.MinX), int(y-compareWall.MinY), cEmpty)
			} else {
				cr, _ := strconv.ParseUint(xrow[y].Color[0:2], 16, 0)
				cg, _ := strconv.ParseUint(xrow[y].Color[2:4], 16, 0)
				cb, _ := strconv.ParseUint(xrow[y].Color[4:6], 16, 0)
				pcol := color.RGBA{R: uint8(cr), G: uint8(cg), B: uint8(cb), A: 255}
				dst.Set(int(x-compareWall.MinX), int(y-compareWall.MinY), pcol)
			}
		}
	}
	return dst, nil
}

func loadComparisonData() bool {
	// If no data pull in the source and parse it
	// If we already have it, check last changed (http 304 or inode number/modification time), no-op if nothing new.

	// Input remote or file?
	re := regexp.MustCompile(`^https?://`)
	if re.MatchString(CompareURL) {
		CompareURLIsURL = true
		updateComparisonData()
	} else {
		// Make sure CompareURL file exists and can be loaded
		if fi, err := os.Stat(CompareURL); err != nil {
			log.Fatalf("CompareURL file %s does not exist! %s\n", CompareURL, err)
			return false
		} else {
			log.Printf("CompareURL file %s of size %d found.\n", CompareURL, fi.Size())
		}
		if !updateComparisonData() {
			log.Fatalf("CompareURL file %s is not working for us :/\n", CompareURL)
			return false
		}
	}
	return true
}

func updateComparisonData() bool {
	newData := &GraffitiWall{}
	if CompareURLIsURL {
		err := getJson(CompareURL, &newData.Data)
		if err != nil {
			log.Fatalf("ERROR: Could not parse comparison data from supplied URL [%s]: %s\n", CompareURL, err)
		}
	} else {
		inputFile, err := os.Open(CompareURL)
		if err != nil {
			log.Fatalf("ERROR: Could not read input for comparison: %s\nPlease supply either a valid path to input JSON file, OR a valid URL, e.g. https://.\n", err)
		}
		defer inputFile.Close()
		data, err := io.ReadAll(inputFile)
		if err != nil {
			log.Fatalf("ERROR: Could not read input for graffiti: %s\nPlease supply either a valid path to input JSON file, OR a valid URL, e.g. https://.\n", err)
		}
		err = json.Unmarshal(data, &newData.Data)
		if err != nil {
			log.Fatalf("ERROR: Could not parse input for graffiti: %s\nPlease supply either a valid path to input JSON file, OR a valid URL, e.g. https://.\n", err)
		}
	}
	// Convert this to map
	compareWall = &CompareWall{}
	compareWall.MinX = 1000
	compareWall.MinY = 1000
	compareWall.MaxX = 0
	compareWall.MaxY = 0
	compareWall.PixelData = make(map[uint]map[uint]GraffitiWallPixel)
	for _, pixel := range newData.Data {
		if compareWall.PixelData[pixel.X] == nil {
			compareWall.PixelData[pixel.X] = make(map[uint]GraffitiWallPixel)
		}
		compareWall.PixelData[pixel.X][pixel.Y] = pixel
		if pixel.X > compareWall.MaxX {
			compareWall.MaxX = pixel.X
		}
		if pixel.Y > compareWall.MaxY {
			compareWall.MaxY = pixel.Y
		}
		if pixel.X < compareWall.MinX {
			compareWall.MinX = pixel.X
		}
		if pixel.Y < compareWall.MinY {
			compareWall.MinY = pixel.Y
		}
	}
	Xoffset = int(compareWall.MinX)
	Yoffset = int(compareWall.MinY)
	log.Printf("Comparedata now holds %d pixels, bounds(%d,%d <-> %d,%d)\n", len(newData.Data), compareWall.MinX, compareWall.MinY, compareWall.MaxX, compareWall.MaxY)
	return true
}

func getJson(url string, target interface{}) error {
	fmt.Printf("Fetching %s for json result....\n", url)
	bhttp := &ohttp.Client{Timeout: 3 * time.Second}

	r, err := bhttp.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	if r.StatusCode < 200 || r.StatusCode > 299 {
		log.Printf("WARNING: Fetch of %s returned unexpected code: %d (%s) - retrying later...\n", url, r.StatusCode, ohttp.StatusText(r.StatusCode))
		return nil
	}
	return json.NewDecoder(r.Body).Decode(target)
}

func fetchGraffitiWall() bool {
	// Fetch graffiti wall from beaconchain and write pixels to local image for comparison
	bgw = &GraffitiWall{}

	err := getJson("https://beaconcha.in/api/v1/graffitiwall", bgw)
	if err != nil {
		log.Fatalln(err)
	}
	if bgw.Status != "OK" {
		fmt.Printf("Unpexected beaconcha.in graffitiwall status: %s\n", bgw.Status)
	} else {
		fmt.Printf("Graffitiwall fetched from beaconcha.in, status: %s\n", bgw.Status)
	}

	// Redraw.
	width := 1000
	height := 1000
	upLeft := image.Point{0, 0}
	lowRight := image.Point{width, height}
	fullWallImage = image.NewRGBA(image.Rectangle{upLeft, lowRight})

	leaderBoard = LeaderBoard{}
	leaderBoard.ValidatorLookup = make(map[uint]*LeaderBoardEntry)

	if lastSlot == 0 {
		log.Printf("Fetching last slot for Pixels per day calc\n")
		updateLastSlot()
		log.Printf("Last slot now %d\n", lastSlot)
	}
	for _, pixel := range bgw.Data {
		cr, _ := strconv.ParseUint(pixel.Color[0:2], 16, 0)
		cg, _ := strconv.ParseUint(pixel.Color[2:4], 16, 0)
		cb, _ := strconv.ParseUint(pixel.Color[4:6], 16, 0)
		pcol := color.RGBA{R: uint8(cr), G: uint8(cg), B: uint8(cb), A: 255}
		fullWallImage.Set(int(pixel.X), int(pixel.Y), pcol)

		// Leaderboard stats
		if leaderBoard.ValidatorLookup[pixel.Validator] == nil {
			leaderBoard.ValidatorLookup[pixel.Validator] = NewLeaderboardEntry(&pixel)
		} else {
			leaderBoard.ValidatorLookup[pixel.Validator].AddPixel(&pixel)
		}
	}
	updateGraffitiStats()
	updateLeaderBoardStats()

	return true
}

func getEtaString() string {
	secondsLeft := math.Round(float64(86400) * float64(fullWallTodoPixels) / float64(pixelsPerDay))
	eta := timediff.TimeDiff(time.Now().Add(time.Duration(secondsLeft) * time.Second))
	log.Printf("getEtaString(): Pixels per day is now %d, eta seconds %.2f, ETA: %s\n", pixelsPerDay, secondsLeft, eta)
	return eta
}

func updateLeaderBoardStats() {
	leaderBoard.RPLValidators = nil
	leaderBoard.Validators = nil
	// Pixels have already been (re)added, so we just need to create the top lists for now
	// and potentially do some lookups for those (e.g. eth1 depositors, ENS, etc)
	for _, lbe := range leaderBoard.ValidatorLookup {
		leaderBoard.Validators = append(leaderBoard.Validators, lbe)
		if lbe.RPLPixelCount > 0 {
			leaderBoard.RPLValidators = append(leaderBoard.RPLValidators, lbe)
		}
	}

	// Sort them by pixels placed
	sort.SliceStable(leaderBoard.Validators, func(i, j int) bool {
		return leaderBoard.Validators[i].PixelCount > leaderBoard.Validators[j].PixelCount
	})
	// Sort RPL ones by RPL pixels placed
	sort.SliceStable(leaderBoard.RPLValidators, func(i, j int) bool {
		return leaderBoard.RPLValidators[i].RPLPixelCount > leaderBoard.RPLValidators[j].RPLPixelCount
	})
}

func NewLeaderboardEntry(pixel *GraffitiWallPixel) *LeaderBoardEntry {
	lbe := &LeaderBoardEntry{
		Validator:        pixel.Validator,
		PixelCount:       0,
		RPLPixelCount:    0,
		BadPixelCount:    0,
		LastSeen:         pixel.Slot,
		AvgBlockDistance: pixel.Slot,
		Pixels:           []*GraffitiWallPixel{},
	}
	lbe.AddPixel(pixel)
	return lbe
}

func (lbe *LeaderBoardEntry) AddPixel(pixel *GraffitiWallPixel) {
	if lbe.Validator != pixel.Validator {
		log.Printf("BUG! lbe.Addpixel called for wrong validator! [%d vs expected %d]", pixel.Validator, lbe.Validator)
		return
	}
	lbe.Pixels = append(lbe.Pixels, pixel)
	isRPL, isMatch := checkPixel(int(pixel.X), int(pixel.Y), pixel.Color)
	if isRPL {
		lbe.RPLPixelCount++
		if !isMatch {
			lbe.BadPixelCount++
		}
	}
	if pixel.Slot > lbe.LastSeen {
		lbe.LastSeen = pixel.Slot
	}
	lbe.PixelCount++

	// Avg block distance for this validator
	firstSlot := pixel.Slot
	lastSlot := pixel.Slot
	for _, vp := range lbe.Pixels {
		if firstSlot > vp.Slot {
			firstSlot = vp.Slot
		}
		if lastSlot < vp.Slot {
			lastSlot = vp.Slot
		}
	}
	lbe.AvgBlockDistance = (lastSlot - firstSlot) / uint(len(lbe.Pixels))
}

func updateGraffitiStats() {
	fullWallPixels = 0     // total pixels in beaconchain wall
	fullWallRPLPixels = 0  // total Rocket Pool pixels in beaconchain wall
	fullWallMisPixels = 0  // total wrong Rocket Pool pixels in beaconchain wall
	fullWallTodoPixels = 0 // total Rocket Pool pixels left to do excluding invalid ones
	pixelsPerDay = 0       // total RPL pixels drawn in the last 7200 slots (24 hours)

	cEmpty := color.RGBA{R: 0, G: 0, B: 0, A: 0}
	bounds := fullWallImage.Bounds()

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			pcol := fullWallImage.At(x, y)
			if pcol != cEmpty {
				pcolR, pcolG, pcolB, _ := pcol.RGBA()
				pcolhex := fmt.Sprintf("%02X%02X%02X", uint8(pcolR), uint8(pcolG), uint8(pcolB))
				isInSpace, isMatch := checkPixel(x, y, pcolhex)
				if isInSpace {
					if isMatch {
						fullWallRPLPixels++
					} else {
						fullWallMisPixels++
					}
				}
				fullWallPixels++
			} else {
				// check todo pixels, if it exists on the compareWall data we need to draw it, unless it's white.
				if compareWall.PixelData[uint(x)] != nil && compareWall.PixelData[uint(x)][uint(y)].Color != "" && compareWall.PixelData[uint(x)][uint(y)].Color != "ffffff" {
					fullWallTodoPixels++
					// fmt.Printf("RPL TODO rx,ry %d,%d mismatch %s is not empty\n", uint(x), uint(y), compareWall.PixelData[uint(x)][uint(y)].Color )
				}
			}
		}
	}

	// Determine Pixels per day, also ppd histogram (note that it uses other boundaries based on UTC days!)
	// slotToYMD(pixel.Slot)  -> 2022-01-01
	var ppd map[string]uint // day [UTC] -> ppd
	ppd = make(map[string]uint)
	ppdkeys := make([]string, 0, len(ppd))

	for _, pixel := range bgw.Data {
		isRPL, isMatch := checkPixel(int(pixel.X), int(pixel.Y), pixel.Color)
		if isRPL && isMatch {
			ymd := slotToYMD(pixel.Slot)
			if _, ok := ppd[ymd]; !ok {
				ppd[ymd] = 0
				ppdkeys = append(ppdkeys, ymd)
			}
			ppd[ymd]++
			if pixel.Slot >= (lastSlot - 7200) {
				pixelsPerDay++
			}
		}
	}

	// Draw ppd histogram!
	log.Printf("PPD Table:\n============\n")
	sort.Strings(ppdkeys)
	// cValues := make([]chart.Value, 0, len(ppd))
	for _, k := range ppdkeys {
		// 	cValues = append(cValues, chart.Value{Value: float64(ppd[k]), Label: k})
		log.Printf("%s [%03d]: %s\n", k, ppd[k], strings.Repeat("*", int(ppd[k])))
	}
	log.Printf("============\n")
	// end of ppd stuff

	fmt.Printf("Stats: %d total pixels drawn, %d Rocket Pool pixels drawn, %d wrong Rocket Pool pixels, %d todo Rocket Pool pixels, %d pixels in the last 24h\n", fullWallPixels, fullWallRPLPixels, fullWallMisPixels, fullWallTodoPixels, pixelsPerDay)
}

func slotToYMD(slot uint) string {
	// Input is a slot number, output is the UTC day-month-year string of that slot's minting based on the genesis (slot 0) being at 2020-12-01 12:00:00 +0000 UTC
	genesis := time.Date(2020, 12, 1, 12, 0, 0, 0, time.UTC)
	target := genesis.Add(time.Second * time.Duration(12*slot))
	// log.Printf("DEBUG: Converted slot %d to timestamp %s\n", slot, target)
	return target.Format("2006-01-02")
}

func checkPixel(x int, y int, c string) (bool, bool) {
	// Checks input x,y,col(RRGGBB) against compareUrl data. Returns (isInRPLSpace, matchesExpectation)

	if compareWall == nil {
		log.Printf("ERROR: Comparison data not available, can't checkPixel!")
		return false, false
	}
	if x > int(compareWall.MaxX) || y > int(compareWall.MaxY) || x < int(compareWall.MinX) || y < int(compareWall.MinY) {
		// this means we are not in RPL space, can still be a valid graffitiwall pixel
		// fmt.Printf("Requested pixel(%d,%d,%s) outside RPL wall image area", x, y, c)
		return false, false
	}
	if compareWall.PixelData[uint(x)] == nil {
		return false, false
	}
	target := compareWall.PixelData[uint(x)][uint(y)]
	if target.Color == "" { // no target
		return false, false
	}
	isMatch := strings.ToUpper(target.Color) == strings.ToUpper(c)
	if !isMatch {
		fmt.Printf("Mismatch pixel, target[%s] but we check against color %s\n", target, c)
	}
	return true, isMatch
}

func getPixelImage(p *gwPixel, bg bool) image.Image {
	// Show the pixel as where it's on the wall. Combine the rpl wallcheck image with the actual pixel drawn and point some arrows to it.
	c := p.hexcolor
	cr, _ := strconv.ParseUint(c[0:2], 16, 0)
	cg, _ := strconv.ParseUint(c[2:4], 16, 0)
	cb, _ := strconv.ParseUint(c[4:6], 16, 0)
	pcol := color.RGBA{R: uint8(cr), G: uint8(cg), B: uint8(cb), A: 255}

	// Copy wallImage first
	b := wallImage.Bounds()
	dst := image.NewRGBA(b)
	// Set background color
	if bg {
		bgcolor := color.RGBA{R: 255, G: 255, B: 255, A: 255}
		draw.Draw(dst, b, &image.Uniform{bgcolor}, image.ZP, draw.Src)
		draw.Draw(dst, b, wallImage, b.Min, draw.Over)
	} else {
		draw.Draw(dst, b, wallImage, b.Min, draw.Src)
	}

	// Draw crosshair around it
	col_o := color.RGBA{R: 240, G: 141, B: 52, A: 255}
	col_r := color.RGBA{R: 243, G: 164, B: 93, A: 255}
	col_b := color.RGBA{R: 0, G: 0, B: 0, A: 255}

	// draw - -, surround in black. i is offset from center
	for i := 3; i < 15; i++ {
		dst.Set(int(p.x)-i, int(p.y)-2, col_b)
		dst.Set(int(p.x)-i, int(p.y)-1, col_o)
		dst.Set(int(p.x)-i, int(p.y)+0, col_r)
		dst.Set(int(p.x)-i, int(p.y)+1, col_o)
		dst.Set(int(p.x)-i, int(p.y)+2, col_b)

		dst.Set(int(p.x)+i, int(p.y)-2, col_b)
		dst.Set(int(p.x)+i, int(p.y)-1, col_o)
		dst.Set(int(p.x)+i, int(p.y)+0, col_r)
		dst.Set(int(p.x)+i, int(p.y)+1, col_o)
		dst.Set(int(p.x)+i, int(p.y)+2, col_b)

		dst.Set(int(p.x)-2, int(p.y)+i, col_b)
		dst.Set(int(p.x)-1, int(p.y)+i, col_o)
		dst.Set(int(p.x)+0, int(p.y)+i, col_r)
		dst.Set(int(p.x)+1, int(p.y)+i, col_o)
		dst.Set(int(p.x)+2, int(p.y)+i, col_b)

		dst.Set(int(p.x)-2, int(p.y)-i, col_b)
		dst.Set(int(p.x)-1, int(p.y)-i, col_o)
		dst.Set(int(p.x)+0, int(p.y)-i, col_r)
		dst.Set(int(p.x)+1, int(p.y)-i, col_o)
		dst.Set(int(p.x)+2, int(p.y)-i, col_b)
	}
	// Black start and ends
	dst.Set(int(p.x)+2, int(p.y)-1, col_b)
	dst.Set(int(p.x)+2, int(p.y)+0, col_b)
	dst.Set(int(p.x)+2, int(p.y)+1, col_b)
	dst.Set(int(p.x)+15, int(p.y)-1, col_b)
	dst.Set(int(p.x)+15, int(p.y)+0, col_b)
	dst.Set(int(p.x)+15, int(p.y)+1, col_b)
	dst.Set(int(p.x)-2, int(p.y)-1, col_b)
	dst.Set(int(p.x)-2, int(p.y)+0, col_b)
	dst.Set(int(p.x)-2, int(p.y)+1, col_b)
	dst.Set(int(p.x)-15, int(p.y)-1, col_b)
	dst.Set(int(p.x)-15, int(p.y)+0, col_b)
	dst.Set(int(p.x)-15, int(p.y)+1, col_b)

	dst.Set(int(p.x)-1, int(p.y)+2, col_b)
	dst.Set(int(p.x)+0, int(p.y)+2, col_b)
	dst.Set(int(p.x)+1, int(p.y)+2, col_b)
	dst.Set(int(p.x)-1, int(p.y)+15, col_b)
	dst.Set(int(p.x)+0, int(p.y)+15, col_b)
	dst.Set(int(p.x)+1, int(p.y)+15, col_b)
	dst.Set(int(p.x)-1, int(p.y)-2, col_b)
	dst.Set(int(p.x)+0, int(p.y)-2, col_b)
	dst.Set(int(p.x)+1, int(p.y)-2, col_b)
	dst.Set(int(p.x)-1, int(p.y)-15, col_b)
	dst.Set(int(p.x)+0, int(p.y)-15, col_b)
	dst.Set(int(p.x)+1, int(p.y)-15, col_b)

	// Draw pixel on it
	dst.Set(int(p.x), int(p.y), pcol)

	return dst
}

func imageToConsole(img image.Image) {
	levels := []string{"  ", "░░", "░▒", "▒▒", "▓▓", "██"}

	bounds := img.Bounds()
	fmt.Printf("Dumping image with bounds %+v\n", bounds)
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			c := color.GrayModel.Convert(img.At(x, y)).(color.Gray)
			level := c.Y / 51 // 51 * 5 = 255
			/*
				if level == 5 {
					level--
				}
			*/
			fmt.Print(levels[level])
		}
		fmt.Print("\n")
	}
}

func (b *BenVBot) sendMessage(msgPub string) {
	b.ds.ChannelMessageSend(bb.channelId, msgPub)
}

func initEth1() {
	// Connect to infura/eth1 client
	log.Printf("Using Execution client URL %s\n", ExecutionURL)
	client, err := ethclient.Dial(ExecutionURL)
	if err != nil {
		panic(err)
	}
	ExecutionClient = client

	// Test connection
	ctx := context.Background()
	blockNo, err := ExecutionClient.BlockNumber(ctx)
	if err != nil {
		log.Fatalf("Could not get latest block number from execution client: %s\n", err)
	}
	peers, _ := ExecutionClient.PeerCount(ctx)
	sync, _ := ExecutionClient.SyncProgress(ctx)
	gas, _ := ExecutionClient.SuggestGasPrice(ctx)
	syncMsg := "is synced"
	if sync != nil {
		syncMsg = "is currently syncing(!)"
	}
	// Gwei -> eth = new(big.Int).Div(value, big.NewInt(params.GWei))

	log.Printf("Connected to Execution client at %s %s, %d peers, last block is %d and gas is set at %d\n", ExecutionURL, syncMsg, peers, blockNo, new(big.Int).Div(gas, big.NewInt(1e9)))
}

func initEth2() {
	// Connect to beaconchain
	fmt.Printf("Using Beacon URL %s\n", BeaconURL)
	ctx := context.Background()
	client, err := http.New(ctx,
		http.WithAddress(BeaconURL),
		http.WithTimeout(10*time.Second),
		http.WithLogLevel(zerolog.WarnLevel),
	)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Connected to %s\n", client.Name())

	// Client functions have their own interfaces.  Not all functions are
	// supported by all clients, so checks should be made for each function when
	// casting the service to the relevant interface.
	if provider, isProvider := client.(eth2client.GenesisProvider); isProvider {
		genesis, err := provider.Genesis(ctx)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Genesis time is %v\n", genesis.GenesisTime)
	}

	if provider, isProvider := client.(eth2client.NodeVersionProvider); isProvider {
		nodeVersion, err := provider.NodeVersion(ctx)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Node version is %v\n", nodeVersion)
	} else {
		fmt.Printf("Node version function NodeVersionProvider missing! No version!\n")
	}

	BeaconClient = client

	// Enable event updates for new blocks
	// For topics, see SupportedEventTopics in https://pkg.go.dev/github.com/attestantio/go-eth2-client@v0.13.4/api/v1
	// topics := []string{"head", "chain_reorg", "block", "attestation"}
	topics := []string{"head"}
	if provider, isProvider := client.(eth2client.EventsProvider); isProvider {
		err := provider.Events(ctx, topics, handleBeaconEvents)
		if err != nil {
			fmt.Printf("Error installing beaconchain event handler: %+v", err)
		} else {
			fmt.Printf("Beacon event handler installed\n")
		}
	} else {
		fmt.Printf("EventProvider function missing! No updates!\n")
	}
	lastFullMessage = time.Now().Unix()
}

func updateLastSlot() {
	ctx := context.Background()
	if provider, isProvider := BeaconClient.(eth2client.SignedBeaconBlockProvider); isProvider {
		block, err := provider.SignedBeaconBlock(ctx, "head")
		if err != nil {
			log.Fatalf("Error fetching head block: %+v", err)
		}
		if block == nil {
			// No such block?
			log.Fatalf("Error fetching head block: no such block?!")
		}
		slotNo, err := block.Slot()
		if err != nil {
			log.Fatalf("Error getting block slot?! %s\n", err)
		}
		lastSlot = uint(slotNo)
		log.Printf("Fetched head block, version %s with slotno %d\n", block.Version, lastSlot)
	}
}

func handleBeaconEvents(event *api.Event) {
	switch event.Topic {
	case "head":
		{
		}
	case "block":
		{
		}
	default:
		{
			fmt.Printf("Beacon %s event! (ignoring)\n", event.Topic)
			return
		}
	}
	bData := event.Data.(*api.HeadEvent)
	block := bData.Slot
	blockNo := fmt.Sprintf("%d", uint64(block))
	graffiti, validatorIndex := getEth2BlockGraffiti(blockNo)
	if i, err := strconv.Atoi(blockNo); err == nil {
		lastSlot = uint(i)
	}
	if len(graffiti) == 0 {
		return
	}
	pixel := findGraffitiWallPixel(graffiti, blockNo, validatorIndex)
	if pixel != nil {
		fullWallImage.Set(int(pixel.x), int(pixel.y), pixel.color)
		log.Printf("Head event, slot now at %s by %d! Graffiti: %s --> PIXEL DETECTED: %s\n", blockNo, validatorIndex, graffiti, pixel)
		oPixel := handlePixel(pixel)
		// bb.sendMessage(fmt.Sprintf("Slot %s by %d places pixel: %s\n", blockNo, validatorIndex, graffiti, pixel))
		// Do we want to broadcast only Rocket Pool pixels, all or none?
		broadcast := false
		switch broadcastMode {
		case 2:
			fallthrough
		default:
			broadcast = true
		case 1:
			broadcast = pixel.isRPL
		case 0:
			broadcast = false
		}
		if broadcast {
			if (time.Now().Unix() - lastFullMessage) > (fullMsgInterval) {
				lastFullMessage = time.Now().Unix()
				sendPixelMsg(bb.ds, pixel, bb.channelId, oPixel)
			} else {
				log.Printf("Sending short pixel message due to lastFullMessage being 'recent', now %d vs lastFullMessage %d -> next broadcast in %d\n", time.Now().Unix(), lastFullMessage, fullMsgInterval-(time.Now().Unix()-lastFullMessage))
				sendPixelMsgShort(bb.ds, pixel, bb.channelId, oPixel)
			}
		} else {
			log.Printf("Not bothering users, broadcast was %t (broadcastMode %d)\n", broadcast, broadcastMode)
		}
	} else {
		log.Printf("Head event, slot now at %s by %d, no pixel. Graffiti: %s\n", blockNo, validatorIndex, graffiti)
		// bb.sendMessage(fmt.Sprintf("slot now at %s! Graffiti: %s\n", blockNo, graffiti))
	}
	// spew.Dump(event)
}

// Internal pixel data structure with more info
type gwPixel struct {
	x         uint16
	y         uint16
	hexcolor  string
	color     *color.RGBA
	isRPL     bool   // in Rocket Pool draw area?
	isOK      bool   // is it the Rocket Pool color we expect?
	slot      string // So we can track validator etc, link to https://beaconcha.in/slot/<slotno>
	validator uint64 // validator index
}

func pixelToGWPixel(p GraffitiWallPixel) *gwPixel {
	return NewPixel(uint16(p.X), uint16(p.Y), p.Color, fmt.Sprintf("%d", p.Slot), uint64(p.Validator))
}

func NewPixel(x uint16, y uint16, hexcolor string, slot string, validator uint64) *gwPixel {
	// parse hexcolor and stuff RGB values in struct
	cr, _ := strconv.ParseUint(hexcolor[0:2], 16, 0)
	cg, _ := strconv.ParseUint(hexcolor[2:4], 16, 0)
	cb, _ := strconv.ParseUint(hexcolor[4:6], 16, 0)
	pcol := &color.RGBA{R: uint8(cr), G: uint8(cg), B: uint8(cb), A: 255}
	isRpl, isCorrect := checkPixel(int(x), int(y), hexcolor)
	return &gwPixel{x, y, hexcolor, pcol, isRpl, isCorrect, slot, validator}
}

func (p GraffitiWallPixel) String() string {
	return fmt.Sprintf("GWPixel(%d, %d, #%s) [validator %d] @%d ", p.X, p.Y, p.Color, p.Validator, p.Slot)
}

func (p gwPixel) String() string {
	msgRpl := "Outside Rocket Pool area"
	msgOk := "n.a."
	if p.isRPL {
		msgRpl = "Part of Rocket Pool Logo!"
		msgOk = "Invalid color"
		if p.isOK {
			msgOk = "Color matches expectation!"
		}
	}
	return fmt.Sprintf("Pixel(%d, %d, #%s) [%s, %s] @%s ", p.x, p.y, p.hexcolor, msgRpl, msgOk, p.slot)
}

func findGraffitiWallPixel(g string, blockNo string, validatorIndex uint64) *gwPixel {
	if len(g) == 0 {
		return nil
	}

	md := map[string]string{}
	switch msg := g; {
	case wallv1.MatchString(msg):
		{
			n1 := wallv1.SubexpNames()
			r2 := wallv1.FindAllStringSubmatch(msg, -1)[0]
			for i, n := range r2 {
				md[n1[i]] = n
			}
		}
	case wallv2.MatchString(msg):
		{
			n1 := wallv2.SubexpNames()
			r2 := wallv2.FindAllStringSubmatch(msg, -1)[0]
			for i, n := range r2 {
				md[n1[i]] = n
			}
		}
	default:
		return nil
	}
	xInt, _ := strconv.ParseUint(md["x"], 10, 16)
	yInt, _ := strconv.ParseUint(md["y"], 10, 16)
	return NewPixel(uint16(xInt), uint16(yInt), md["hexcolor"], blockNo, validatorIndex)
}

func getEth2Version() string {
	ctx, cancel := context.WithCancel(context.Background())
	if provider, isProvider := BeaconClient.(eth2client.NodeVersionProvider); isProvider {
		nodeVersion, err := provider.NodeVersion(ctx)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Node version is %v\n", nodeVersion)
		cancel()
		return nodeVersion
	}
	cancel()
	return "? (error)"
}

// Block for testing graffitiwall: 2823711
func getEth2BlockGraffiti(blockID string) (string, uint64) {
	ctx, cancel := context.WithCancel(context.Background())
	if provider, isProvider := BeaconClient.(eth2client.SignedBeaconBlockProvider); isProvider {
		//  (*spec.VersionedSignedBeaconBlock, error) -> https://pkg.go.dev/github.com/attestantio/go-eth2-client@v0.13.4/spec#VersionedSignedBeaconBlock
		block, err := provider.SignedBeaconBlock(ctx, blockID)
		if err != nil {
			fmt.Printf("Error fetching block %s: %+v", blockID, err)
			cancel()
			return "", 0
		}
		if block == nil {
			// No such block?
			fmt.Printf("Error fetching block %s: no such block", blockID)
			cancel()
			return "", 0
		}
		graffiti := ""
		validatorIndex := uint64(0)
		switch block.Version {
		case spec.DataVersionAltair:
			graffiti = string(bytes.Trim(block.Altair.Message.Body.Graffiti[:], "\x00"))
			validatorIndex = uint64(block.Altair.Message.ProposerIndex)
		case spec.DataVersionBellatrix:
			graffiti = string(bytes.Trim(block.Bellatrix.Message.Body.Graffiti[:], "\x00"))
			validatorIndex = uint64(block.Bellatrix.Message.ProposerIndex)
		default:
			graffiti = string(bytes.Trim(block.Phase0.Message.Body.Graffiti[:], "\x00"))
			validatorIndex = uint64(block.Phase0.Message.ProposerIndex)
		}
		// spew.Dump(block)
		cancel()
		return fmt.Sprintf("%s", graffiti), validatorIndex
	}

	cancel()
	fmt.Printf("No SignedBeaconBlock provider!?")
	return "? (go-eth2-client library failure error)", 0
}

func main() {
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		log.Fatalf("Error creating Discord session: %v", err)
		return
	}
	bb = &BenVBot{ds: dg, channelId: ChannelID}

	if listenMode {
		log.Printf("Listen mode activated, registering for Guild Messages")
		// Register the messageCreate func as a callback for MessageCreate events.
		dg.AddHandler(messageCreate)
		// We only care about receiving message events in this example.
		dg.Identify.Intents = discordgo.IntentsGuildMessages
	}

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		log.Fatalf("Error opening discord connection: %v", err)
		return
	}

	// Register our slash commands
	for _, v := range commands {
		_, err := dg.ApplicationCommandCreate(dg.State.User.ID, GuildID, v)
		if err != nil {
			log.Printf("WARNING: Cannot create '%v' command using GuildID %s: %v", v.Name, GuildID, err)
		}
	}
	dg.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if h, ok := commandHandlers[i.ApplicationCommandData().Name]; ok {
			h(s, i)
		}
	})

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("BenVBot is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Remove our commands when we're not there
	for _, v := range commands {
		err := dg.ApplicationCommandDelete(dg.State.User.ID, GuildID, v.ID)
		if err != nil {
			log.Printf("ERROR: Cannot delete '%v' command: %v", v.Name, err)
		}
	}

	// Cleanly close down the Discord session.
	dg.Close()
}

func handlePixel(pixel *gwPixel) *gwPixel {
	// This function handles new pixels being drawn (e.g. update stats, leaderboard, etc)
	// Returns the old pixel if any got overwritten, nil otherwise

	var oPixel *gwPixel
	// Update bgw with pixel
	pcolR, pcolG, pcolB, _ := pixel.color.RGBA()
	pcolhex := fmt.Sprintf("%02X%02X%02X", uint8(pcolR), uint8(pcolG), uint8(pcolB))
	slot, _ := strconv.ParseUint(pixel.slot, 10, 0)
	newPixel := GraffitiWallPixel{
		pcolhex,
		uint(slot),
		uint(pixel.validator),
		uint(pixel.x),
		uint(pixel.y),
	}
	log.Printf("DEBUG: Converted pixel %s ---- to %s\n", pixel, newPixel)
	// See if this pixel was already in the BGW data
	for _, oldPixel := range bgw.Data {
		if oldPixel.X == newPixel.X && oldPixel.Y == newPixel.Y {
			log.Printf("*** PIXEL OVERWRITE ****, OLD[%s] ==> NEW[%s]", oldPixel, newPixel)
			if oldPixel.Color == newPixel.Color {
				log.Printf(" -> worse: ***** PIXEL REDRAW ****")
			}
			oPixel = pixelToGWPixel(oldPixel)
		}
	}
	bgw.Data = append(bgw.Data, newPixel)

	// Add pixel to Leaderboard
	if leaderBoard.ValidatorLookup[newPixel.Validator] == nil {
		leaderBoard.ValidatorLookup[newPixel.Validator] = NewLeaderboardEntry(&newPixel)
	} else {
		leaderBoard.ValidatorLookup[newPixel.Validator].AddPixel(&newPixel)
	}

	// New pixel, new stats.
	updateLeaderBoardStats()
	updateGraffitiStats()
	getEtaString() // just for debug

	return oPixel
}

func sendPixelMsgShort(s *discordgo.Session, pixel *gwPixel, channel string, oldPixel *gwPixel) {
	validator := getValidator(pixel.validator)
	pixelcheck := "▢"
	if pixel.isRPL {
		if pixel.isOK {
			pixelcheck = "✅"
		} else {
			pixelcheck = "❌"
		}
	}
	msgColor := int(pixel.color.R)<<16 + int(pixel.color.G)<<8 + int(pixel.color.B)
	embed := &discordgo.MessageEmbed{
		Description: fmt.Sprintf("RPL Pixel %d painted in slot [%s](https://beaconcha.in/slot/%s) by [%d](https://beaconcha.in/validator/%d)/[%s](https://rocketscan.io/address/%s)  colored #%s %s", fullWallRPLPixels, pixel.slot, pixel.slot, pixel.validator, pixel.validator, validator.NodeName, validator.NodePubkey, strings.ToUpper(pixel.hexcolor), pixelcheck),
		Type:        discordgo.EmbedTypeRich,
		Color:       msgColor, // pixel value
	}
	dmsg := &discordgo.MessageSend{
		Embed: embed,
	}
	_, err := s.ChannelMessageSendComplex(channel, dmsg)
	if err != nil {
		fmt.Printf("ERROR trying to send short message to channel %s: %s\n", channel, err)
	} else {
		fmt.Println("Short Pixel message sent...")
	}
}

func sendPixelMsg(s *discordgo.Session, pixel *gwPixel, channel string, oldPixel *gwPixel) {
	gspt.SetProcTitle("BenVBot [drawing]")
	fmt.Println("Generating pixel preview...")
	wimg := getPixelImage(pixel, false)
	gifName := "wallupdate.gif"
	pfpName := "evil.png"
	validator := getValidator(pixel.validator)

	// Let's try animated png
	b := wallImage.Bounds()
	dst := image.NewRGBA(b)
	draw.Draw(dst, b, fullWallImage, b.Min, draw.Src)
	draw.Draw(dst, b, wimg, b.Min, draw.Over)

	wfullWallImage := image.NewRGBA(b)
	bgcolor := color.RGBA{R: 255, G: 255, B: 255, A: 255}
	draw.Draw(wfullWallImage, b, &image.Uniform{bgcolor}, image.ZP, draw.Src)
	draw.Draw(wfullWallImage, b, fullWallImage, b.Min, draw.Over)

	/* // Unil discord shows this properly we won't be using this. Tough.
	a := apng.APNG{
		Frames: []apng.Frame{
			{
				Image:            wfullWallImage,
				DelayNumerator:   1,
				DelayDenominator: 2,
			},
			{
				Image:            dst,
				DelayNumerator:   1,
				DelayDenominator: 2,
			},
		},
	}

	// png.Encode(&fbuf, wimg)
	apng.Encode(&fbuf, a) // Fscking discord doesn't preview this :-(
	*/

	pngName := "rplwall.png"
	fbuf := makeRpWallImage()

	subimages := []image.Image{
		wfullWallImage,
		dst,
	}

	// The default Gif quantizer is apparently unable to do transparent, let's help it with a white version with everything on there
	draw.Draw(dst, b, wfullWallImage, b.Min, draw.Src)
	draw.Draw(dst, b, wimg, b.Min, draw.Over)

	// Try gif. It's terribly slow but at least discord properly displays it.
	outGif := &gif.GIF{}
	// quantizer := quantize.MedianCutQuantizer{NumColor: 64}
	// quantizer := quantize.MedianCutQuantizer{}
	// p := make([]color.Color, 0, 256)
	for _, simage := range subimages {
		bounds := simage.Bounds()
		pImg := image.NewPaletted(bounds, palette.Plan9)
		draw.Draw(pImg, bounds, simage, image.ZP, draw.Src)

		// Add new frame to animated GIF
		outGif.Image = append(outGif.Image, pImg)
		outGif.Delay = append(outGif.Delay, 50) // in 100ths of a second, see https://pkg.go.dev/image/gif#GIF
	}
	var gbuf bytes.Buffer // In-memory buffer to store images
	gif.EncodeAll(&gbuf, outGif)
	f := bytes.NewReader(fbuf.Bytes())
	g := bytes.NewReader(gbuf.Bytes())
	h := bytes.NewReader(iconBuffer.Bytes())

	pixelcheck := "▢"
	if pixel.isRPL {
		if pixel.isOK {
			pixelcheck = "✅"
		} else {
			pixelcheck = "❌"
		}
	}
	msgColor := int(pixel.color.R)<<16 + int(pixel.color.G)<<8 + int(pixel.color.B)
	fmt.Printf("Color: %s -> int value %d\n", pixel.hexcolor, msgColor)
	eta := getEtaString()
	embed := &discordgo.MessageEmbed{
		// Timestamp:   time.Now().Format(time.RFC3339), // Discord wants ISO8601; RFC3339 is an extension of ISO8601 and should be completely compatible.
		Title:       "Graffiti Wall Pixel locator",
		Description: fmt.Sprintf("Slot [%s](https://beaconcha.in/slot/%s) by [%d](https://beaconcha.in/validator/%d)/[%s](https://rocketscan.io/address/%s) #%s %s", pixel.slot, pixel.slot, pixel.validator, pixel.validator, validator.NodeName, validator.NodePubkey, strings.ToUpper(pixel.hexcolor), pixelcheck),
		Type:        discordgo.EmbedTypeImage,
		// Color: 0xE67E22, // Orange
		Color: msgColor, // pixel value
		Image: &discordgo.MessageEmbedImage{
			// URL: "data:image/png;base64," + f64, // discord doesn't like this :/
			URL:    "attachment://" + pngName, // can not use data:image/png;base64,<base64data>
			Width:  1000,
			Height: 1000,
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL:    "attachment://" + gifName,
			Width:  1000,
			Height: 1000,
		},
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "Total Pixels",
				Value:  fmt.Sprintf("%d", fullWallPixels),
				Inline: true,
			},
			{
				Name:   "RPL Pixels ✅",
				Value:  fmt.Sprintf("%d", fullWallRPLPixels),
				Inline: true,
			},
			{
				Name:   "RPL Pixels ❌",
				Value:  fmt.Sprintf("%d", fullWallMisPixels),
				Inline: true,
			},
			{
				Name:   "RPL Pixels TODO",
				Value:  fmt.Sprintf("%d, %.2f%% done", fullWallTodoPixels, float64(float64(100.0)-(float64(fullWallTodoPixels)/float64(20.4)))),
				Inline: true,
			},
			{
				Name:   "Last 24h",
				Value:  fmt.Sprintf("%d px, 🏁 %s", pixelsPerDay, eta),
				Inline: true,
			},
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "hacked together by BenV / trink.eth\n",
			IconURL: "attachment://" + pfpName,
		},
	}

	// Slot 4622659 by 362096 places Pixel(680, 469, #ff4f36) [Part of Rocket Pool Logo!, Color matches expectation!] @4622659
	msgRpl := "outside the Rocket Pool drawing area"
	msgOk := ""
	if pixel.isRPL {
		msgRpl = "part of the Rocket Pool logo"
		msgOk = "and has an unexpected color?"
		if pixel.isOK {
			msgOk = "and the color matches expectation!"
		}
	}
	if oldPixel != nil {
		slot, _ := strconv.ParseUint(oldPixel.slot, 10, 0)
		if oldPixel.hexcolor != pixel.hexcolor {
			msgOk += fmt.Sprintf("\nThis paints over an [already drawn pixel](https://beaconcha.in/slot/%d).", slot)
		} else {
			msgOk += fmt.Sprintf("\nNOTE: This pixel had [already been drawn](https://beaconcha.in/slot/%d) with exactly the same color.", slot)
		}
	}
	pixelMsg := fmt.Sprintf("pixel(%d, %d, %s) which is %s %s", pixel.x, pixel.y, pixel.hexcolor, msgRpl, msgOk)
	dmsg := &discordgo.MessageSend{
		Content: fmt.Sprintf("Slot %s by %d places %s", pixel.slot, pixel.validator, pixelMsg),
		Embed:   embed,
		Files: []*discordgo.File{
			{
				Name:        pngName,
				Reader:      f,
				ContentType: "image/png",
			},
			{
				Name:        gifName,
				Reader:      g,
				ContentType: "image/gif",
			},
			{
				Name:        pfpName,
				Reader:      h,
				ContentType: "image/png",
			},
		},
	}
	_, err := s.ChannelMessageSendComplex(channel, dmsg)
	if err != nil {
		fmt.Printf("ERROR trying to send message to channel %s: %s\n", channel, err)
	} else {
		fmt.Println("Pixel message sent...")
	}
	gspt.SetProcTitle("BenVBot [main]")
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// fmt.Printf("messageCreate: %+v", m)
	// spew.Dump(m)
	fmt.Printf("%s %15.15s: %s\n", m.Timestamp, m.Author, m.Content)
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	msgPub := ""
	msgPriv := ""

	var block = regexp.MustCompile(`^block\s+(?P<blockno>\d+)`)

	switch msg := m.Content; {
	case msg == "timelapse":
		dmsg := generateTimelapseMessage()
		_, err := s.ChannelMessageSendComplex(m.ChannelID, dmsg)
		if err != nil {
			fmt.Printf("ERROR trying to send message to channel %s: %s\n", m.ChannelID, err)
		} else {
			fmt.Println("Timelapse message sent...")
		}
		return
	case msg == "version":
		msgPub = fmt.Sprintf("Node version: %s", getEth2Version())
	case wallv1.MatchString(msg):
		fallthrough
	case wallv2.MatchString(msg):
		pixel := findGraffitiWallPixel(msg, "#User", 0)
		if pixel != nil {
			// sendPixelMsg(s, pixel, m.ChannelID, nil)
			sendPixelMsgShort(s, pixel, m.ChannelID, nil)
		} else {
			msgPub = fmt.Sprintf("Graffiti: %s did not get a pixel? Bug!\n", msg)
		}
	case block.MatchString(msg):
		// Go regexp is the most amazing tool ever *cough* *cough*
		n1 := block.SubexpNames()
		r2 := block.FindAllStringSubmatch(msg, -1)[0] // TODO: improve this garbage
		md := map[string]string{}
		for i, n := range r2 {
			md[n1[i]] = n
		}

		blockNo := md["blockno"]
		graffiti, validatorIndex := getEth2BlockGraffiti(blockNo)
		if len(graffiti) == 0 {
			msgPub = fmt.Sprintf("Block %s has no graffiti. Lame\n", blockNo)
		} else {
			pixel := findGraffitiWallPixel(graffiti, blockNo, validatorIndex)
			if pixel != nil {
				msgPub = fmt.Sprintf("Block %s by %d: Graffiti: %s -- PIXEL DETECTED: %s\n", blockNo, validatorIndex, graffiti, pixel)
				oPixel := handlePixel(pixel)
				sendPixelMsg(s, pixel, m.ChannelID, oPixel)
			} else {
				msgPub = fmt.Sprintf("Block %s by %d: Graffiti: %s -- no pixel found\n", blockNo, validatorIndex, graffiti)
			}
		}
	// Skip messages we don't care about
	default:
		return
	}

	// We create the private channel with the user who sent the message.
	if len(msgPriv) > 0 {
		channel, err := s.UserChannelCreate(m.Author.ID)
		if err != nil {
			// If an error occurred, we failed to create the channel.
			//
			// Some common causes are:
			// 1. We don't share a server with the user (not possible here).
			// 2. We opened enough DM channels quickly enough for Discord to
			//    label us as abusing the endpoint, blocking us from opening
			//    new ones.
			fmt.Println("error creating channel:", err)
			s.ChannelMessageSend(
				m.ChannelID,
				"Something went wrong while sending the DM!",
			)
			return
		}
		// Then we send the message through the channel we created.
		_, err = s.ChannelMessageSend(channel.ID, msgPriv)
		if err != nil {
			// If an error occurred, we failed to send the message.
			//
			// It may occur either when we do not share a server with the
			// user (highly unlikely as we just received a message) or
			// the user disabled DM in their settings (more likely).
			fmt.Println("error sending DM message:", err)
			s.ChannelMessageSend(m.ChannelID, "Failed to send you a DM. Did you disable DM in your privacy settings?")
		}
	}
	if len(msgPub) > 0 {
		s.ChannelMessageSend(m.ChannelID, msgPub)
	}
}
